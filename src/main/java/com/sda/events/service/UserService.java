package com.sda.events.service;

import com.sda.events.model.User;

import java.util.List;

public interface UserService {

    void save (User user);

    User findByUsername(String username);

    List<User> getAllUsers();
}
