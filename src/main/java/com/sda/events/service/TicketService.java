package com.sda.events.service;

import com.sda.events.model.Ticket;

public interface TicketService {

    void save (Ticket ticket);

    Ticket findByTicketId(String ticketId);
}
