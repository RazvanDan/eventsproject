package com.sda.events.service;

import com.sda.events.model.Artist;

import java.util.List;

public interface ArtistService {

    void save (Artist artist);

    Artist findByName(String name);

     List<Artist> getAllArtists();

}
