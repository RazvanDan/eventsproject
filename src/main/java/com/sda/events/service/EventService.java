package com.sda.events.service;

import com.sda.events.model.Event;

import java.util.List;

public interface EventService {

    void save (Event event);

    Event findByEventName (String eventName);

     List<Event> getAllEvents();
}
