package com.sda.events.service;


import com.sda.events.model.Event;
import com.sda.events.repository.EventRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public void save (Event event){
        eventRepository.save(event);
    }

    @Override
    public Event findByEventName(String eventName){
        return eventRepository.findByEventName(eventName);
    }

    public EventRepository getEventRepository() {
        return eventRepository;
    }

    public void setEventRepository(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }


    public List<Event> getAllEvents(){
        return eventRepository.findAll();
    }
}


