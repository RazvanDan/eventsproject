package com.sda.events.service;

import com.sda.events.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;


public interface RoleService {

    void save (Role role);

    Role findByRole(String role);

     List<Role> getAllRoles();
}
