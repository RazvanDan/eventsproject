package com.sda.events;

import com.sda.events.controller.EventController;
import com.sda.events.model.*;
import com.sda.events.repository.UserRepository;
import com.sda.events.service.EventService;
import com.sda.events.service.RoleServiceImpl;
import com.sda.events.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import com.sda.events.service.ArtistService;
import java.util.Calendar;

@SpringBootApplication
@ComponentScan(basePackages = {"com.sda.events.controller", "com.sda.events.service"})
public class EventsApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

//
//    @Autowired
//    private ArtistService artistService;
//
//    @Autowired
//    private EventService eventService;



    public static void main(String[] args) {
        SpringApplication.run(EventsApplication.class, args);


    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User("razvan7200", "password", "Razvan", "Dan", "razvan@dan.ro");
        User user2 = new User("razvan4000", "parola", "Loti", "Turbatul", "loti@turbatul.ro");
        User user3 = new User("random", "nouaparola", "Ion", "Popescu", "ion@popescu.ro");

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);

        Role role1 = new Role("admin");
        Role role2 = new Role("customer");

        //roleService.save(role1);
        //roleService.save(role2);

        Event event1 = new Event("Untold", "Apahida",Calendar.getInstance().getTime());
        //eventService.save(event1);

        Ticket ticket1 = new Ticket(1, 100);

    }

//    public void setRoleService(RoleServiceImpl roleService) {
//        this.roleService = roleService;
//    }



}
