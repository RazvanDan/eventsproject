package com.sda.events.controller;

import com.sda.events.model.Event;
import com.sda.events.model.User;
import com.sda.events.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/clist")
    public String listUsers(ModelMap model){
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        return "";

}}
