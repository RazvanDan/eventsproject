package com.sda.events.controller;


import com.sda.events.model.Event;
import com.sda.events.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping("/test")
    public String lists(Map<String, Object> model){
        List<Event> events = eventService.getAllEvents();
        model.put("events", events);
        return "test";
    }

    @RequestMapping("/eventsList")
    public String list(Map<String, Object> model){
        List<Event> events = eventService.getAllEvents();
        model.put("events", events);
        return "eventsList";
    }


}
