package com.sda.events.controller;


import com.sda.events.model.Artist;
import com.sda.events.repository.ArtistRepository;
import com.sda.events.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ArtistController {

    @Autowired
    private ArtistService artistService;

    @RequestMapping("/dlist")
    public String listArtists(ModelMap model){
        List<Artist> artists = artistService.getAllArtists();
        model.addAttribute("artists", artists);
        return "list";
    }
}
