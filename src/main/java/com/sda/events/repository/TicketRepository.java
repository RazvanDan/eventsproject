package com.sda.events.repository;

import com.sda.events.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

     Ticket findByTicketId(String ticketId);
}
