package com.sda.events.repository;

import com.sda.events.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {

    Event findByEventName(String eventName);

}
