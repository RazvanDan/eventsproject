package com.sda.events.repository;

import com.sda.events.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer>{

    Role findByRoleId (String roleId);

}
