package com.sda.events.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "event")
public class Event {
    public Event() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="EVENT_ID")
    private int eventId;

    @Column(name="NAME")
    private String eventName;

    @Column(name="LOCATION")
    private String location;

    @Column(name="DATE_TIME")
    private Date dateAndTime;


//    private Set<Ticket> tickets;
    @ManyToMany
    private Set<Event> events;

    public Event(String name, String location, Date dateAndTime) {

        this.eventName = name;
        this.location = location;
        this.dateAndTime = dateAndTime;
        //this.tickets = tickets;
        //this.events = events;

    }


    public String getName() {
        return eventName;
    }

    public void setName(String name) {
        this.eventName = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }
//
//    public Set<Ticket> getTickets() {
//        return tickets;
//    }
//
//    public void setTickets(Set<Ticket> tickets) {
//        this.tickets = tickets;
//    }

//    public Set<Event> getEvents() {
//        return events;
//    }
//
//    public void setEvents(Set<Event> events) {
//        this.events = events;
//    }
}
