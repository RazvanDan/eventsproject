package com.sda.events.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name="artist")
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="artist_id")
    private int artistId;


    @Column(name="artist_name")
    private String name;

    @Column(name="artist_genre")
    private String genre;

    @Column(name="artist_description")
    private String description;

    @Column(name="artist_facebooklink")
    private String facebookLink;

    @Column(name="artist_soundclicklink")
    private String soundCloudLink;


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "artist_event",
            joinColumns = { @JoinColumn(name = "artist_id")},
            inverseJoinColumns = { @JoinColumn(name = "event_id")}
            )
    private Set<Event> events;

    public Artist(String name, String genre, String description, String facebookLink, String soundCloudLink) {
        this.name = name;
        this.genre = genre;
        this.description = description;
        this.facebookLink = facebookLink;
        this.soundCloudLink = soundCloudLink;
        //this.events = events;
    }

    public Artist() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getSoundCloudLink() {
        return soundCloudLink;
    }

    public void setSoundCloudLink(String soundCloudLink) {
        this.soundCloudLink = soundCloudLink;
    }

//    public Set<Event> getEvents() {
//        return events;
//    }
//
//    public void setEvents(Set<Event> events) {
//        this.events = events;
//    }
}
