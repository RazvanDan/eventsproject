package com.sda.events.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ROLE_ID")
    private int roleId;

    @Column(name="ROLE_TYPE")
    private String roleType;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public Role( String roleType) {
        this.roleType = roleType;
    }

    public Role() {
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }




}
